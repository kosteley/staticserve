package staticserve;

import spark.Spark;

/**
 * Created by gosha on 1/28/17.
 */
public class Main {

  public static void main(String[] args) {
    Spark.externalStaticFileLocation(args[0]);
    if(args.length> 1) {
      Spark.port(Integer.valueOf(args[1]));
    }
    Spark.init();

    Spark.awaitInitialization();
  }
}
